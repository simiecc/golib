package monitor

import (
	"time"
)

type MonitorBase struct {
	//	Id          int64
	Timestamp   time.Time `json:"timestamp"`
	Temperature float32   `json:"temperature"`
	Humidity    float32   `json:"humidity"`
}
