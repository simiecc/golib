package cslog

import (
	"context"
	"io"
	"path"
	"runtime"
	"strconv"
	"sync"
	"time"

	"log/slog"
)

type Handler struct {
	WithColor bool
	MinLevel  slog.Level
	AddSource bool

	W io.Writer

	mu sync.Mutex
}

func (h *Handler) Enabled(_ context.Context, l slog.Level) bool {
	return l >= h.MinLevel
}

// WithAttrs returns a new TextHandler whose attributes consists
// of h's attributes followed by attrs.
func (h *Handler) WithAttrs(attrs []slog.Attr) slog.Handler {
	// return &TextHandler{commonHandler: h.commonHandler.withAttrs(attrs)}
	// TODO
	return h
}

func (h *Handler) WithGroup(name string) slog.Handler {
	// return &TextHandler{commonHandler: h.commonHandler.withGroup(name)}
	return h
}

func (h *Handler) Handle(_ context.Context, r slog.Record) error {
	// return h.commonHandler.handle(r)

	buf := newBuffer()
	defer buf.Free()

	if h.WithColor {
		buf.WriteString("\033[0m")
	}

	if !r.Time.IsZero() {
		val := r.Time.Round(0) // strip monotonic to match Attr behavior
		// buf.WriteString(val.Format("2006-01-02 15:04:05.000"))
		writeTime(buf, val)
		buf.WriteByte(' ')
	}

	// level
	switch r.Level {
	case slog.LevelInfo:
		buf.WriteString("\033[1;32m")
	case slog.LevelWarn:
		buf.WriteString("\033[1;35m")
	case slog.LevelError:
		buf.WriteString("\033[1;31m")
	default:
		buf.WriteString("\033[m")
	}
	buf.WriteString(r.Level.String())
	if r.Level == slog.LevelInfo || r.Level == slog.LevelWarn {
		buf.WriteByte(' ')
	}
	buf.WriteByte(' ')

	// message
	buf.WriteString("\033[0;37m")
	buf.WriteString(r.Message)
	buf.WriteByte(' ')

	// state.groups = stateGroups // Restore groups passed to ReplaceAttrs.
	// preformatted Attrs
	// if len(s.h.preformattedAttrs) > 0 {
	// 	s.buf.WriteString(s.sep)
	// 	s.buf.Write(s.h.preformattedAttrs)
	// 	s.sep = s.h.attrSep()
	// }
	// Attrs in Record -- unlike the built-in ones, they are in groups started
	// from WithGroup.
	// s.prefix = buffer.New()
	// defer s.prefix.Free()
	// s.prefix.WriteString(s.h.groupPrefix)
	// s.openGroups()
	r.Attrs(func(a slog.Attr) bool {
		buf.WriteString("\033[1;36m")
		buf.WriteString(a.Key)
		buf.WriteByte('=')
		buf.WriteString("\033[m")
		buf.WriteString(a.Value.String())
		buf.WriteByte(' ')
		return true
	})

	// source
	if h.AddSource {
		fs := runtime.CallersFrames([]uintptr{r.PC})
		frame, _ := fs.Next()

		if frame.File != "" {
			buf.WriteString("\033[1;30m")
			buf.WriteString(path.Base(frame.File))
			buf.WriteByte(':')
			buf.WriteString(strconv.Itoa(frame.Line))
			// buf.WriteByte(' ')
		}
	}

	buf.WriteString("\033[m\n")

	h.mu.Lock()
	defer h.mu.Unlock()
	_, err := h.W.Write(*buf)

	return err
}

func writeTime(buf *buffer, t time.Time) {
	year, month, day := t.Date()
	buf.WritePosIntWidth(year, 4)
	buf.WriteByte('-')
	buf.WritePosIntWidth(int(month), 2)
	buf.WriteByte('-')
	buf.WritePosIntWidth(day, 2)
	buf.WriteByte(' ')
	hour, min, sec := t.Clock()
	buf.WritePosIntWidth(hour, 2)
	buf.WriteByte(':')
	buf.WritePosIntWidth(min, 2)
	buf.WriteByte(':')
	buf.WritePosIntWidth(sec, 2)
	ns := t.Nanosecond()
	buf.WriteByte('.')
	buf.WritePosIntWidth(ns/1e6, 3)
	// _, offsetSeconds := t.Zone()
	// if offsetSeconds == 0 {
	// 	buf.WriteByte('Z')
	// } else {
	// 	offsetMinutes := offsetSeconds / 60
	// 	if offsetMinutes < 0 {
	// 		buf.WriteByte('-')
	// 		offsetMinutes = -offsetMinutes
	// 	} else {
	// 		buf.WriteByte('+')
	// 	}
	// 	buf.WritePosIntWidth(offsetMinutes/60, 2)
	// 	buf.WriteByte(':')
	// 	buf.WritePosIntWidth(offsetMinutes%60, 2)
	// }
}
