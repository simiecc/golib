package traceByHttp

import (
	"context"
	"runtime"
	"strings"

	"github.com/gin-gonic/gin"
)

func StartTraceHttp(parentCtx context.Context, listenAddr string) {
	engine := gin.Default()
	engine.GET("/trace", handleFullTrace)
	go func() {
		engine.Run(listenAddr)
	}()
}

func handleFullTrace(c *gin.Context) {

	var b = make([]byte, 10240)
	runtime.Stack(b, true)
	c.String(200, strings.Trim(string(b), " \x00"))
}
