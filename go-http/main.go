// Example from Go wasm into https://github.com/golang/go/wiki/WebAssembly#getting-started
// A basic HTTP server.
// By default, it serves the current working directory on port 8080.
package main

import (
	"flag"
	"log"
	"net/http"
)

var (
	listen = flag.String("listen", ":8080", "listen address")
	dir    = flag.String("dir", ".", "directory to serve")
)

//func loggingHandler
type LoggingHandler struct {
	target http.Handler
}

func NewLoggingHandler(handler http.Handler) http.Handler {
	return &LoggingHandler{
		target: handler,
	}
}

func (lh *LoggingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var data Data
	lh.target.ServeHTTP(LoggingResponseWriter{
		data:   &data,
		target: w,
	}, r)

	if data.outputBytes > 0 {
		log.Printf("%v %v - %v %v", r.Method, r.URL, data.status, data.outputBytes)
	} else {
		log.Printf("%v %v - %v", r.Method, r.URL, data.status)

	}
}

type Data struct {
	status      int
	outputBytes uint
}

type LoggingResponseWriter struct {
	data   *Data
	target http.ResponseWriter
}

func (lw LoggingResponseWriter) Header() http.Header {
	return lw.target.Header()
}

func (lw LoggingResponseWriter) Write(b []byte) (int, error) {
	lw.data.outputBytes += uint(len(b))
	return lw.target.Write(b)
}

func (lw LoggingResponseWriter) WriteHeader(statusCode int) {
	lw.data.status = statusCode
	lw.target.WriteHeader(statusCode)
}

func main() {
	flag.Parse()
	log.Printf("listening on %q...", *listen)

	handler := NewLoggingHandler(http.FileServer(http.Dir(*dir)))
	err := http.ListenAndServe(*listen, handler)
	log.Fatalln(err)
}
