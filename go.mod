module gitlab.com/simiecc/golib

go 1.12

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.19.1
)
