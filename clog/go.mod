module gitlab.com/simiecc/golib/clog

require (
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	go.uber.org/zap v1.21.0
	golang.org/x/lint v0.0.0-20191125180803-fdd1cda4f05f // indirect
)

go 1.13
