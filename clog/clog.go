package clog

import (
	"fmt"
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func init() {
	zap.ReplaceGlobals(NewDevelopment())
}

func S() *zap.SugaredLogger {
	return zap.S()
}

func Replace(newLogger *zap.Logger) {
	zap.ReplaceGlobals(newLogger)
}

// func InitFileLogger(filePath string) Logger {
// 	config := zap.NewDevelopmentConfig()
// 	config.OutputPaths = []string{filePath}
// 	rawlogger, err := config.Build(zap.AddCaller())
// 	if err != nil {
// 		log.Fatalf("can't initialize zap logger: %v", err)
// 	}

// 	slogger := rawlogger.Sugar()

// 	return slogger
// }

func MyTimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.Local().Format("2006-01-02T15:04:05.999"))
}

func NewDevelopment() *zap.Logger {
	config := zap.NewDevelopmentConfig()
	config.EncoderConfig.EncodeTime = MyTimeEncoder

	rawlogger, err := config.Build(zap.AddCallerSkip(1))
	if err != nil {
		fmt.Println("Can't initialize zap logger: ", err)
		os.Exit(1)
	}

	return rawlogger
}

func NewProduction(filePathName string) *zap.Logger {
	return NewProductionConfig(filePathName, func(config *zap.Config) {})
}

func NewProductionConfig(filePathName string, configFn func(*zap.Config)) *zap.Logger {

	config := zap.NewDevelopmentConfig()
	config.OutputPaths = []string{"stdout", filePathName}
	configFn(&config)

	rawlogger, err := config.Build(zap.AddCallerSkip(1))
	if err != nil {
		fmt.Println("Can't initialize zap logger: ", err)
		os.Exit(1)
	}

	return rawlogger
}

/** Wrapper for zap logger */
func Desugar() *zap.Logger {
	return S().Desugar()
}

func Named(name string) *zap.SugaredLogger {
	return S().Named(name)
}

func With(args ...interface{}) *zap.SugaredLogger {
	return S().With(args...)
}

func Debug(args ...interface{}) {
	S().Debug(args...)
}

func Info(args ...interface{}) {
	S().Info(args...)
}

func Warn(args ...interface{}) {
	S().Warn(args...)
}

func Error(args ...interface{}) {
	S().Error(args...)
}

func DPanic(args ...interface{}) {
	S().DPanic(args...)
}

func Panic(args ...interface{}) {
	S().Panic(args...)
}

func Fatal(args ...interface{}) {
	S().Fatal(args...)
}

func Debugf(template string, args ...interface{}) {
	S().Debugf(template, args...)
}

func Infof(template string, args ...interface{}) {
	S().Infof(template, args...)
}

func Warnf(template string, args ...interface{}) {
	S().Warnf(template, args...)
}

func Errorf(template string, args ...interface{}) {
	S().Errorf(template, args...)
}

func DPanicf(template string, args ...interface{}) {
	S().DPanicf(template, args...)
}

func Panicf(template string, args ...interface{}) {
	S().Panicf(template, args...)
}

func Fatalf(template string, args ...interface{}) {
	S().Fatalf(template, args...)
}

func Debugw(msg string, keysAndValues ...interface{}) {
	S().Debugw(msg, keysAndValues...)
}

func Infow(msg string, keysAndValues ...interface{}) {
	S().Infow(msg, keysAndValues...)
}

func Warnw(msg string, keysAndValues ...interface{}) {
	S().Warnw(msg, keysAndValues...)
}

func Errorw(msg string, keysAndValues ...interface{}) {
	S().Errorw(msg, keysAndValues...)
}

func DPanicw(msg string, keysAndValues ...interface{}) {
	S().DPanicw(msg, keysAndValues...)
}

func Panicw(msg string, keysAndValues ...interface{}) {
	S().Panicw(msg, keysAndValues...)
}

func Fatalw(msg string, keysAndValues ...interface{}) {
	S().Fatalw(msg, keysAndValues...)
}

func Sync() error {
	return S().Sync()
}
